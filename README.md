# Aprendendo Common Lisp

Minhas anotações no estudo de Common Lisp. Este é um estudo aberto a todos que queiram participar dele, tendo sempre em mente que eu mesmo estou aprendendo do zero. Cada página de anotações termina com uma seção de "Colaborações", onde comentários e acréscimos podem ser publicados por *pull request*. Dúvidas, correções e demais discussões devem ser publicadas nas [issues do repositório](https://codeberg.org/blau_araujo/aprendendo-clisp/issues).

## Fontes e material de estudo

- [**Practical Common Lisp**, de Peter Seibel (livro na web)](https://gigamonkeys.com/book/)
- [**GNU Common Lisp by Examples** (site)](http://www.csci.viu.ca/~wesselsd/courses/csci330/code/lisp/)
- [**CLiki, the common lisp wiki** (site)](https://www.cliki.net/)
- [**Learn Common Lisp** (site)](https://lisp-lang.org/learn/)
- [**Common Lisp the Language, 2nd Edition**, de Guy L. Steele Jr. (livro na web)](http://www.cs.cmu.edu/Groups/AI/html/cltl/clm/clm.html)
- [**Successful Lisp**, de David B. Lamkins (livro na web)](https://dept-info.labri.fr/~strandh/Teaching/MTP/Common/David-Lamkins/cover.html)

## Índice

**[1 - Por que aprender Lisp?](notas/01-intro.md)**

- [1.1 - Origens](notas/01-intro.md#1-1-origens)
- [1.2 - Quais são os objetivos do nosso estudo?](notas/01-intro.md#1-2-quais-s%C3%A3o-os-objetivos-do-nosso-estudo)
- [1.3 - Qual implementação nós utilizaremos?](notas/01-intro.md#1-3-qual-implementa%C3%A7%C3%A3o-n%C3%B3s-utilizaremos)
- [1.4 - Como configuar um ambiente de estudo?](notas/01-intro.md#1-4-como-configuar-um-ambiente-de-estudo)
- [1.5 - Ambientes integrados](notas/01-intro.md#1-5-ambientes-integrados)
- [Colaborações](notas/01-intro.md#colabora%C3%A7%C3%B5es)
