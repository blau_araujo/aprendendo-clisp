# Aprendendo Common Lisp

> Anotações feitas a partir da leitura da [versão na web do livro "Practical Common Lisp", de Peter Seibel](https://gigamonkeys.com/book/).

## 1 - Por que aprender Lisp?

A compensação de programar em Common Lisp tem muito menos a ver com critérios objetivos de produtividade ou aplicabilidade -- todos atendidos perfeitmanete pela linguagem -- e muito mais a ver com a experiência de programar em uma linguagem de programação que permite que ela mesma seja programada. Como diz [Peter Seibel, em *Practical Common Lisp*](https://gigamonkeys.com/book/):

> *"Mais do que qualquer outra linguagem, o Common Lisp segue a filosofia de o que é bom para o projetista da linguagem é bom para o utilizador da linguagem. Logo, quando estiver programando em Common Lisp, você quase nunca se pegará desejando que a linguagem supoortasse algum recurso que facilitaria a escrita do seu programa, porque é possível simplesmente incluir o recurso você mesmo."*

### 1.1 - Origens

O Common Lisp descende da linguagem Lisp que foi concebida por John McCarthy, em 1956, para processamento de dados simbólicos. O nome "Lisp" vem de *LISt Processing* ("processamento de listas", em português). Por volta de 1986, as primeiras implementações do Common Lisp já estavam disponíveis com o propósito de substituir toda uma série de dialetos do Lisp que, graças ao seu poder para criar novas linguagens, proliferaram desde seu surgimento. Mesmo tendo suplantado a maioria dos dialetos Lisp de quem se originou, o Common Lisp não é o único dialeto remanescente. Portanto, é preciso levar em conta o dialeto em questão quando alguém fala de Lisp.

Um dos dialetos mais "basais" ainda em uso nos dias de hoje é o Scheme. O Common Lisp importou muitas ideias do Scheme sem nunca pretender substituí-lo. O Scheme sempre manteve o foco de ser uma linguagem simples, reduzida em termos de recursos: o que é bom para ensinar Lisp, mas ao custo de muitas funcionalidades padronizadas no Common Lisp. Consequentemente, é muito mais difícil escrever programas portáveis em Scheme do que em Common Lisp.

Outro dialeto bastante difundido até hoje é o Elisp (*Emacs Lisp*). Embora muita coisa tenha sido escrita em Elisp, talvez mais do que em qualquer outro dialeto, sua aplicação depende do Emacs e o estilo geral da linguagem é bem menos contemporâneo se comparado com o Scheme ou o Common Lisp.

### 1.2 - Quais são os objetivos do nosso estudo?

O principal objetivo, mas não o único, é fazer a transição de *"alguma noção de Lisp"*, obtida majoritariamente ao lidar com Elisp no Emacs, para o conhecimento de um dialeto Lisp mais robusto, padronizado e de intenso uso prático na atualidade para desenvolvimento de software. Para tanto, nos interessa conhecer os conceitos fundamentais, a sintaxe e a semântica da linguagem e, principalmente, descobrir como implementar soluções para problemas computacionais do mundo real em Common Lisp.

### 1.3 - Qual implementação nós utilizaremos?

Uma das características mais importantes do Common Lisp, é que a liguagem é definida por um padrão. Não existe uma implementação única controlada por uma pessoa ou uma corporação: todos são livres para ler o padrão é criar a sua implementação do Common Lisp. Além disso, as mudanças no padrão têm que ser feitas de acordo com o Instituno Nacional Americano de Padrões (ANSI). Deste modo, o padrão do Common Lisp é um tipo de contrato entre os desenvolvedores dessas implementações e os programadores, onde é garantido que, se o programa utilizar algo da linguagem descirto no padrão, o programa se comportará da mesma forma em qualquer implementação.

Isso não quer dizer que todas as implementações serão iguais ou estarão limitadas ao que está descrito no padrão. O compromisso do "contrato" diz respeito apenas ao uso daquilo que estiver descrito, mas não cobre aquilo que as implementações porventura oferecerem a mais. Dependendo do projeto, pode valer a pena utilizar uma ou outra implementação a partir de seus recursos. Por outro lado, se a ideia for compartilhar código com outras pessoas, o ideal é escrever códigos portáveis em Common Lisp.

Para nós, o requisito mais fundamental é a liberdade da computação, o que nos leva a buscar pelas implementações distribuídas sob licenças livres, como a *GNU Common Lisp* (GCL), a GNU CLISP e a *Steel Bank Common Lisp* (SBCL): todas as três opções facilmente encontradas na forma de pacotes na maioria das distribuições GNU/Linux.

### 1.4 - Como configuar um ambiente de estudo?

Tendo instalado qualquer uma das implementações citadas, a rigor, nós só precisamos de um editor de textos e um terminal (ou um editor de textos que seja executado no terminal). Pessoalmente, quando se trata de ensinar e aprender programação, esta é a minha preferência, pois ela permite que toda atenção se concentre no aprendizado da linguagem.

Além disso, todas as implementações citadas oferecem uma espécie de "shell" onde podemos executar códigos em Common Lisp de forma interativa: o chamado REPL (de *read-eval-print loop*).

> De todos, por padrão, o REPL do GNU CLISP é, de longe, o melhor para uso no terminal do GNU/Linux.

Do ambiente do REPL, nós podemos:

- Definir e alterar elementos de programas;
- Avaliar expressões em Lisp;
- Carregar arquivos fonte de programas em Lisp;
- Carregar programas em Lisp compilados;
- Compilar programas em Lisp;
- Compilar funções individualmente;
- Debugar o código;
- Inspecionar estados de objetos...

Tudo isso e muito mais graças às funções definidas por padrão na linguagem.

### 1.5 - Ambientes integrados

Com um pouco mais de esforço, nós podemos utilizar o Emacs como um ambiente de estudo (e até de desenvolvimento) que integra um editor de textos, um REPL e uma infinidade de recursos úteis para gerenciamento de projetos e controle de versões. Configurar e aprender a utilizar o Emacs pode ser uma complicação no começo, mas nós podemos adiar esse passo utilizando algumas das distribuições do Emacs previamente configuradas para o desenvolvimento em Common Lisp. Dentre elas, destaca-se a distribuição [Portacle](https://portacle.github.io/).

Portacle é uma IDE para Common Lisp que não requer instalação: os arquivos são extraídos em um diretório onde nós só temos que entrar e executar um script para abrir o Emacs configurado para uso como ambiente integrado de estudo e desenvolvimento.

Com a distribuição, nós teremos:

- O Emacs com os pacotes Slime, Magit e Company instalados;
- A implementação Common Lisp SBCL;
- O gerenciador de pacotes Lisp Quicklisp;
- E o gerenciador de versões Git.


## Colaborações

Através de *pull requests*, editando a partir da linha divisória abaixo, deixe suas notas e comentários para expandir os tópicos apresentados. Para organizar a colaboração, vamos manter o seguinte padrão de edição:

```
### Sobre...

Por @colaborador

> Citação do trecho comentado...

Nota ou comentário....

---
```

É importante incluir a linha de separação (`---`) ao final da colaboração.

---
